//
//  AppDelegate.h
//  gfdghfdgh
//
//  Created by Pete Nelson on 12/02/2013.
//  Copyright (c) 2013 Mixture Ltd. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
